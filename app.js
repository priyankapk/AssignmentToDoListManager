var app = angular.module("app", ['ui.router']);
app.controller("LoginCtrl", function($scope,$state){
    $scope.loginDetails = {};
    $scope.listDetails = {};
    $scope.dashboard = false;
    getToDos();
    $scope.login= function(loginDetails){
        if($scope.loginDetails.username === "admin" && $scope.loginDetails.password === "admin" ){
            console.log("logged in");
            $scope.dashboard = true;
        }
    }
    $scope.toDos=[];
    $scope.addList = function(listDetails){
        $scope.toDos.push($scope.listDetails.title);
        console.log($scope.toDos);
        localStorage.setItem("todos", JSON.stringify($scope.toDos));
        $scope.listDetails ={};
        getToDos();

    }
    function getToDos() {
        try {
            $scope.toDos = JSON.parse(localStorage.getItem("todos"));
            return $scope.toDos.length == 0 ? [] : $scope.toDos;
        } catch (e) {
            $scope.toDos = [];
        }
    }
    $scope.removeItem = function(item){
        var index= $scope.toDos.indexOf(item);
        $scope.toDos.splice(index,1);
        if ($scope.toDos.length == 0) {
            localStorage.clear();
        } else {
            localStorage.setItem("todos", JSON.stringify($scope.toDos));
        }
    }

});